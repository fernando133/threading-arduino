 #include "pt.h"

static int LED_1 = 13;
static int LED_2 = 12;
static int LED_3 = 11;
int button = 2;


static long LED_1_BLINK =  200; //500 milliseconds
static long LED_2_BLINK = 100; //1 Second

static struct pt pt1, pt2;

void led_toggle( int led )
{
  digitalWrite( led, !digitalRead( led ) );
}


static int thread1( struct pt *pt, int led, long timeout ) {
  static long t1 = 0;
  PT_BEGIN( pt );
  while(1) {
    PT_WAIT_UNTIL( pt, (millis() - t1) > timeout );
    led_toggle( led );
    t1 = millis();
  }
  PT_END( pt );
}

static int thread2( struct pt *pt, int led, long timeout ) {
  static long t2 = 0;
  PT_BEGIN( pt );
  while(1) {
    PT_WAIT_UNTIL( pt, (millis() - t2) > timeout );
    led_toggle( led );
    t2 = millis();
  }
  PT_END( pt );
}

void setup() {
  
  pinMode( LED_1, OUTPUT );
  pinMode( LED_2, OUTPUT );
  PT_INIT( &pt1 );
  PT_INIT( &pt2 );
}

void loop() {
  thread1( &pt1, LED_1, LED_1_BLINK );
  thread2( &pt2, LED_2, LED_2_BLINK );
}

